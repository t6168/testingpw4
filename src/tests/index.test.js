const { DEFAULT } = require('../constants');
const { userInput, displayTasklist } = require('../helper')
const { Task } = require('../models/Task')

let tasklist = new Map()
let count = 0
let task

describe("Unit test", () => {
  wrongInput()
  taskListOperations()
  formatDisplay()
});

function wrongInput() {
  describe("[USER SYMBOLS]", () => {
    test("testing user input for stopping", () => {
      expect(userInput("q", tasklist)).toEqual('exit');
    });

    test("testing number of parameters : empty string should return an error", () => {
      const error = userInput("", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing number of parameters : wrong number of parameter should return an error", () => {
      const error = userInput("+", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing number of parameters : wrong operator format should return an error", () => {
      const error = userInput("a Learn Python", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing first parameter reversed", () => {
      const error = userInput("Learn Python +", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing number of parameters : wrong format for the first element should return an error", () => {
      const error = userInput("a abcd", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing number of parameters : wrong format for the first element should should return an error", () => {
      const error = userInput("'' abcd", tasklist)
      expect(error.code).toEqual("02");
    });

    test("testing parameters format : wrong id format for minus operator should return an error", () => {
      const error = userInput("- abcd", tasklist)
      expect(error.code).toEqual("02");
    });

    test("testing parameters format : wrong id format for done operator should return an error", () => {
      const error = userInput("x abcd", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing parameters format : wrong id format for to do operator should return an error", () => {
      const error = userInput("o abcd", tasklist)

      expect(error.code).toEqual("02");
    });

    test("testing parameters format : given char that is not allowed when adding a task ('#')", () => {
      const error = userInput("+ #", tasklist)

      expect(error.code).toEqual("02");
    });
    test("testing parameters format : given second arg that is not allowed when adding a task", () => {
      const error = userInput("+ reqreza # rteztre .", tasklist)

      expect(error.code).toEqual("02");
    });
  });
}


function taskListOperations() {
  describe("[UPDATE TASKLIST] - Verify tasklist operations", () => {
    test("Verify the task has been inserted in the taskList", () => {
      task = new Task("adding task", count, DEFAULT.STATUS.TODO)
      const id = userInput("+ adding task", tasklist)

      expect(tasklist.get(id).equal(task)).toBeTruthy()
    });
    test("Verify the task status can be set to 'done'", () => {
      const id = userInput("x 0", tasklist)

      expect(tasklist.get(id).equal(task)).toBeFalsy()
      expect(tasklist.get(id).getStatus()).toEqual(DEFAULT.STATUS.DONE);
    });

    test("Verify the task status can be set to 'to do'", () => {
      const id = userInput("o 0", tasklist)

      expect(tasklist.get(id).equal(task)).toBeTruthy()
      expect(tasklist.get(id).getStatus()).toEqual(DEFAULT.STATUS.TODO);
    });

    test("Verify the task has been removed in the taskList", () => {
      const object = userInput("- 0", tasklist)

      expect(object.equal(task)).toBeTruthy()
      expect(tasklist.has(1)).toBeFalsy()
      count = 0
    });
    test("Verify that the task list doesn't contains two times the same object", () => {
      tasklist.clear()
      tasklist.set(0, new Task("adding task", 0, DEFAULT.STATUS.TODO))
      count += 1

      const error = userInput("+ adding task", tasklist)

      expect(error.code).toEqual("03");
      count = 0
    });
  });
}

function formatDisplay() {
  describe("[FORMATED DISPLAY] - Verify the format of the display", () => {
    test("Verify the display when the tasklist is empty", () => {
      tasklist.clear()
      const formatedDisplay = displayTasklist(tasklist)

      expect(formatedDisplay).toEqual("No task yet");
    });
    test("Verify the display when the tasklist contains an element", () => {
      const id = userInput("+ Learn Python", tasklist)
      const formatedDisplay = displayTasklist(tasklist)

      task = tasklist.get(id)

      expect(formatedDisplay).toEqual(`${task.getId()} [${task.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${task.getDescription()}`);
      count += 1
    });

    test("Verify the display when the tasklist contains more than one element", () => {
      const id = userInput("+ Learn Java", tasklist)
      const formatedDisplay = displayTasklist(tasklist)

      const task2 = tasklist.get(id)

      expect(formatedDisplay).toEqual(`${task.getId()} [${task.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${task.getDescription()}\n${task2.getId()} [${task2.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${task2.getDescription()}`);
      count += 1
    });

    test("Verify the display when the tasklist contains more than one element with at least one element marked as 'done'", () => {
      const id = userInput("x 1", tasklist)
      const formatedDisplay = displayTasklist(tasklist)

      const task2 = tasklist.get(count)

      expect(formatedDisplay).toEqual(`${task.getId()} [${task.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${task.getDescription()}\n${task2.getId()} [${task2.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${task2.getDescription()}`);
    });
  });
}