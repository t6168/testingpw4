const DEFAULT = {
    STATUS: {
        TODO: "To do",
        DONE: "Done"
    },
    MSG: {
        PLUS: "Add a task",
        MINUS: "Remove task with ID",
        DONE: "Task marked done",
        TODO: "Task marked to do",
        EXIT: "exit",
        NO_TASK : "No task yet"
    },
    ELEMENTS : ['+', '-', 'x', 'o', 'q'],
    ERRORS : {
        BAD_INPUT : { code : "01", msg : "Input must be a 'string'"},
        BAD_FIRST_ARGUMENT : { code : "02", msg : "Operator missing or first arguments is not an operator"},
        BAD_SECOND_ARGUMENT : { code : "02", msg : "A second argument must be provided"},
        BAD_SECOND_ARGUMENT_FORMAT : { code : "02", msg : "Please provide a second argument with only one space and alphanumeric character"},
        BAD_SECOND_ARGUMENT_FORMAT_INT : { code : "02", msg : "Please provide a valid 'int'"},
        BAD_SECOND_ARGUMENT_FORMAT_STR : { code : "02", msg : "Please provide a valid 'string'"},
        WRONG_TASK_ID : { code : "02", msg : "Verify the id of the task"},
        WRONG_OPERATOR : { code : "02", msg : "Unrecognized operator"},
        TASK_ALREADY_EXIST : { code : "03", msg : "You can't add a task that already exist"}
    }
}

module.exports = {
    DEFAULT
}