let prompt = require("prompt");
let colors = require("colors/safe");

const { DEFAULT } = require('../constants');
const { Task } = require('../models/Task')
var validator = require('validator');

prompt.message = ">>>"
prompt.delimiter = ":"
prompt.start()

let count = 0

/**
 * @description This function is the main function of the program.
 */
async function main() {
    let cond = false
    let taskList = new Map()

    while (cond != true) {
        console.log(displayTasklist(taskList))
        const { choice } = await prompt.get(['choice']);

        const res = userInput(choice, taskList)

        if (res.code != undefined) {
            console.log(colors.red.underline(`Error Code : ${res.code} msg : ${res.msg}`))
        } else {
            if (res == DEFAULT.MSG.EXIT) {
                console.log("Bye!")
                cond = true
            }
        }
    }
}

/**
 * @description this function is used to make a decision based on the user input's.
 * 
 * @param {string} input it represents the input given by the user e.g : ('+ Learn Python').
 * @param {Map} taskList it is the task list that store the to do list.
 * @returns {int | Error | Task} Based on the user input an `id` , a `Task` and an `Error` can be returned.
 */
function userInput(input, taskList) {
    if (!alowedType(input, "string")) {
        return DEFAULT.ERRORS.BAD_INPUT;
    }

    const parts = input.split(' ');
    const operator = parts[0]

    if (!DEFAULT.ELEMENTS.includes(parts[0])) {
        return DEFAULT.ERRORS.BAD_FIRST_ARGUMENT;
    }

    if (operator != DEFAULT.ELEMENTS[4]) {
        if (!isSecondArgsValid(parts.slice(1).join(""))) { return DEFAULT.ERRORS.BAD_SECOND_ARGUMENT_FORMAT }

        let secondArg = parts.slice(1).join(" ");
        if (secondArg == []) { return DEFAULT.ERRORS.BAD_SECOND_ARGUMENT; }

        switch (operator) {
            case DEFAULT.ELEMENTS[0]:
                if (!alowedType(secondArg, "string")) {
                    return DEFAULT.ERRORS.BAD_SECOND_ARGUMENT_FORMAT_STR;
                }

                if (alreadyInTaskList(taskList, secondArg)) { return DEFAULT.ERRORS.TASK_ALREADY_EXIST }

                taskList.set(count, new Task(secondArg.toLocaleLowerCase(), count, DEFAULT.STATUS.TODO))
                count += 1

                return count - 1
            case DEFAULT.ELEMENTS[1]:
                secondArg = (validator.isInt(secondArg) ? parseInt(secondArg) : "02")

                if (secondArg === "02") {
                    return DEFAULT.ERRORS.BAD_SECOND_ARGUMENT_FORMAT_INT;
                }

                if (!taskList.has(secondArg)) {
                    return DEFAULT.ERRORS.WRONG_TASK_ID
                } else {
                    task = taskList.get(secondArg)
                    taskList.delete(secondArg)

                    return task
                }
            case DEFAULT.ELEMENTS[2]:
                secondArg = (validator.isInt(secondArg) ? parseInt(secondArg) : "02")

                if (secondArg === "02") {
                    return DEFAULT.ERRORS.BAD_SECOND_ARGUMENT_FORMAT_INT;
                }

                if (!taskList.has(secondArg)) {
                    return DEFAULT.ERRORS.WRONG_TASK_ID
                } else {
                    task = taskList.get(secondArg)

                    task.setStatus(DEFAULT.STATUS.DONE)
                    taskList.set(task.getId(), task)

                    return task.getId()
                }
            case DEFAULT.ELEMENTS[3]:
                secondArg = (validator.isInt(secondArg) ? parseInt(secondArg) : "02")

                if (secondArg === "02") {
                    return DEFAULT.ERRORS.BAD_SECOND_ARGUMENT_FORMAT_INT;
                }

                if (!taskList.has(secondArg)) {
                    return DEFAULT.ERRORS.WRONG_TASK_ID
                } else {
                    task = taskList.get(secondArg)

                    task.setStatus(DEFAULT.STATUS.TODO)
                    taskList.set(task.getId(), task)

                    return task.getId()
                }
            default:
                return DEFAULT.ERRORS.WRONG_OPERATOR;
        }
    } else {
        return DEFAULT.MSG.EXIT
    }
}

/**
 * @description This function is used to perform a filter based on the type of the object given.
 * 
 * @param {object} param It represents the object that we want to keep or throw.  
 * @param {string} accepted It represents the string representation of the type accepted. 
 * @returns {boolean} If the param is the type of the string given in parameter `True` will be returned otherwise `False` will be returned.
 */
function alowedType(param, accepted) {
    return typeof param == accepted
}

/**
 * @description This function is used to verify based on the description if a task is already in the task list with the same description.
 * 
 * @param {Map} taskList it is the task list that store the to do list.
 * @param {string} description It represents the description we want to verify if it is already in the task list.
 * @returns {boolean} If an existing task have the same description `True` will be returned otherwise `False` will be returned.
 */
function alreadyInTaskList(taskList, description) {
    ok = false
    taskList.forEach((value) => {
        if (value.getDescription().toLocaleLowerCase() == description.toLocaleLowerCase()) {
            ok = true
        }
    })
    return ok
}

/**
 * @description This function is used to return a string containing all the task string representation.
 * 
 * @param {Map} taskList it is the task list that store the to do list.
 * @returns {string} Either the `string representation of the task list` or the following string `"No task yet"`.
 */
function displayTasklist(taskList) {
    let ans = "", cpt = 0
    if (taskList.size == 0) { return DEFAULT.MSG.NO_TASK }

    taskList.forEach((value, key) => {
        ans += (cpt == taskList.size - 1) ? `${key} [${value.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${value.getDescription().toLocaleLowerCase()}` : `${key} [${value.getStatus() == DEFAULT.STATUS.DONE ? "X" : " "}] ${value.getDescription().toLocaleLowerCase()}\n`
        cpt += 1
    })

    return ans
}

/**
 * @description This function is used to verify the content of after the operator given. The allowed content must be an alphanumeric string with one space between elements.
 * 
 * @param {string} input It represent the string after the operator given by the user we want to verify if the content is allowed.
 * @returns {boolean} If the input contains only alphanumeric chars `True` will be returned otherwise `False` will be returned.
 */
function isSecondArgsValid(input) {

    if(!alowedType(input, "string")) {return false;}

    const res = input.split(" ");
    let ans = 0;

    res.forEach((el) => (ans += validator.isAlphanumeric(el, "fr-FR")));

    return res.length == ans;
};

module.exports = {
    main, userInput, displayTasklist
}