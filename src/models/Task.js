class Task {
    constructor(description, id, status) {
        this._description = description
        this._id = id
        this._status = status
    }

    getDescription() { return this._description }
    getId() { return this._id }
    getStatus() { return this._status }

    setDescription(description) { this._description = description }
    setid(id) { this._id = id }
    setStatus(status) { this._status = status }

    equal(task){
        const areObjValid = (this instanceof Task && task instanceof Task);

        if(!areObjValid){ return false}

        if(this === task){return true}

        if(this._description.toLowerCase() != task._description.toLowerCase()){return false}
        if(this._status != task._status){return false}

        return true
    }

    displayObject() {
        return `[${this._id}] - [${this._status}] ${this._description}`
    }
}

module.exports = {
    Task
};
