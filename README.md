# TestingPW4

This repository is made to work on a software testing pratical work about TDD.

## How to run the program ?

The first step is to clone this repository either using the ssh or https way. After this step you must perform `cd testingpw4` to be at the root of the project folder.
You are now able to perform a `npm i` to install all the required libraries. When all the libraries are donwloaded you can run the project by using the following command
`npm run start`. 

## How to run the tests ?

The first step is to clone this repository either using the ssh or https way. After this step you must perform `cd testingpw4` to be at the root of the project folder.
You are now able to perform a `npm i` to install all the required libraries. When all the libraries are donwloaded you can run tests of  the project by using the following command
`npm run test`. 

## To-do

1.  Parse user input (+, -, x, o)
- [X] When we are creating a task all the properties must exists
- [X] If a symbol is detected we print the corresponding task
- [X] Maximum of two parameters entered by the user
- [X] Verify the first element is an operator
- [X] Verify if the second element exist
- [X] Verify the second element format
2.  Update the task list
- [X] Verify the task has been inserted in the taskList
- [X] Verify the task has been removed in the taskList
- [X] Verify that the task list doesn't contains two times the same object
3.  Display the tasks
- [X] Verify the format of the display.
4.  Run the interaction loop

## Authors and acknowledgment

[CLEMENCEAU Tristan](https://gitlab.com/tristann)
[JOBE Seedy](https://gitlab.com/seedyjobe)
[ROMAIN Alex](https://gitlab.com/alexdan.romain)

## Links

[Subject](https://github.com/dmerejkowsky/kata-task-manager)
